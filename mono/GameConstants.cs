﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CarBot
{
    public static class MathTools
    {
        public static double DegToRad(double degrees)
        {
            return Math.PI * degrees / 180.0;
        }
    }

    public static class GameConstants
    {
        // public static double Ug = 0.31985;
        public static List<double> GravitySamples = new List<double>();
        public static double Gravity = 12; // Original was 16, but lower the default so don't crash on first corner
        public static double Acceleration = 0.2;
        public static double Friction = 0.02;
        public static double MaxAngle = 50;

        public static double VelocityFactor = 0.3278855026;
        public static double Velocity2Factor = 0.0633234256;

        public static double SpringVelocityFactor = 0.00125;
        public static double SpringAvFactor = 0.094;
        

        public static double Ug { get { return Gravity * Friction; } }
    }

    public class Accelerometer
    {
        private const int StateCount = 2;

        private struct AcState
        {
            public int Tick;
            public double Position;
            public double? Velocity;
            public double? Acceleration;
        }

        private readonly AcState[] _states = new AcState[StateCount];
        private int _prevStateIndex = -1;

        public Accelerometer()
        {
            for (var i = 0; i < StateCount; i++)
            {
                _states[i].Tick = -1;
            }
        }

        public void SetPosition(int tick, double position)
        {
            var index = (_prevStateIndex + 1) % (StateCount);
            _states[index].Tick = tick;
            _states[index].Position = position;

            if (_prevStateIndex > -1)
            {
                var dt = tick - _states[_prevStateIndex].Tick;

                if (dt > 0)
                {
                    _states[index].Velocity = (position - _states[_prevStateIndex].Position) / (double)dt;

                    if (_states[_prevStateIndex].Velocity.HasValue)
                    {
                        _states[index].Acceleration = (_states[index].Velocity - _states[_prevStateIndex].Velocity) /
                                                      (double)dt;
                    }
                    else
                    {
                        _states[index].Acceleration = null;
                    }
                }
                else
                {
                    _states[index].Velocity = null;
                    _states[index].Acceleration = null;
                }
            }

            _prevStateIndex = index;
        }

        public double Velocity
        {
            get
            {
                if (_prevStateIndex == -1) return 0;

                return _states
                    .Where(s => s.Tick != -1 && s.Velocity.HasValue)
                    .Select(s => s.Velocity.Value)
                    .DefaultIfEmpty()
                    .Average();
            }
        }

        public double Acceleration
        {
            get
            {
                if (_prevStateIndex == -1) return 0;

                return _states
                    .Where(s => s.Tick != -1 && s.Acceleration.HasValue)
                    .Select(s => s.Acceleration.Value)
                    .DefaultIfEmpty()
                    .Average();
            }
        }

        public void Reset()
        {
            for (var i = 0; i < StateCount; i++)
            {
                if (i == _prevStateIndex)
                {
                    _states[i].Position = 0;
                }
                else
                {
                    _states[i] = new AcState() { Tick = -1 };
                }
            }
        }
    }
}
