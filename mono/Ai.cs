﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Resources;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using Newtonsoft.Json;

namespace CarBot
{
    public class Plan
    {
        public bool UseTurbo;
        public int ThrottleTicks;
        public int TargetLane;
        public int TargetLane2;

        public double Distance;
        public double DistanceTraveled;
        public int TicksTraveled;
        // public double CausedCrashes;

        public Plan(bool useTurbo, int throttleTicks, int targetLane, int targetLane2)
        {
            UseTurbo = useTurbo;
            ThrottleTicks = throttleTicks;

            TargetLane = targetLane;
            TargetLane2 = targetLane2;
        }

        public override string ToString()
        {
            return string.Format("TT: {0}, {1}->{2}, d={3}", ThrottleTicks, TargetLane, TargetLane2, Distance);
        }
    }

    public class Ai : IAi
    {
#if LOG
        private readonly List<GameState> _simStates = new List<GameState>();
#endif
        protected readonly IGame _game;
        protected Race _race;
        protected CarIdentifier _myCar;
        private GameState _predictedState;

        protected Dictionary<string, Accelerometer> _accelerometers;
        private Dictionary<string, Accelerometer> _anglometers;
        protected Dictionary<string, CarPosition> _prevPositions;
        protected Dictionary<string, double> _traveledDistance;

        private double _throttle;
        private int _targetLane;
        private bool _turboAvailable;
        private TurboData _turboData;
        private int _turboTicksRemaining;
        private double _maxAngleForLap = 0;

        #region Constructors

        public Ai(IGame game)
        {
            _game = game;
        }

        #endregion

        #region Properies

        protected double Throttle
        {
            get { return _throttle; }
            set
            {
                if (Math.Abs(_throttle - value) > 0.00001)
                {
                    _throttle = value;
                    // _throttleSetAt = _game.Tick;
                }
            }
        }

        #endregion

        public void Save()
        {
#if LOG
            string str = JsonConvert.SerializeObject(_simStates);
            File.WriteAllText(@"c:\tmp\sim.txt", str);

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("g=" + GameConstants.Gravity);
            sb.AppendLine("a=" + GameConstants.Acceleration);
            sb.AppendLine("f=" + GameConstants.Friction);
            File.WriteAllText(@"c:\tmp\constants.txt", sb.ToString());
#endif
        }

        public void Joined(string name, string key)
        {
            Console.WriteLine("Joined");
        }

        public void YourCar(CarIdentifier identifier)
        {
            Console.WriteLine("YourCar");
            _myCar = identifier;
        }

        public void GameInit(Race race)
        {
            Console.WriteLine("GameInit "  + race.RaceSession.Laps + " laps, " + race.Cars.Count + " cars ");

            _race = race;
            _maxAngleForLap = 0;
            InitializeMeters();
        }

        public void GameStart()
        {
            Console.WriteLine("GameStart");

            _predictedState = null;
            _turboAvailable = false;
            _turboData = null;
            _turboTicksRemaining = 0;
            _maxAngleForLap = 0;

            Throttle = 1;
            _game.Throttle(Throttle);
        }

        public virtual void CarPositions(List<CarPosition> positions, bool reply)
        {
            try
            {
                if (_turboTicksRemaining > 0) _turboTicksRemaining--;
                if (_race == null || _race.Track == null || _race.Track.Lanes == null) return;

                var carIndex = _race.Cars.FindIndex(c => c.Id.Color.Equals(_myCar.Color));
                var pos = positions.First(cp => cp.Id.Color == _myCar.Color);
                var prevPos = _prevPositions[_myCar.Color];

                if (Math.Abs(pos.Angle) > _maxAngleForLap)
                {
                    _maxAngleForLap = Math.Abs(pos.Angle);
                }

                // Update meters
                UpdateMeters(positions);

                if (!reply) return;

                // Run the constant detection
                DetectConstants(prevPos, pos);

                // At start -> full throttle (Should actually be set at gamestart)
                if (_game.Tick <= 3)
                {
                    Throttle = 1.0;
                    _game.Throttle(Throttle);
                    return;
                }

                // Create simulator
                var sim = new Simulator(_race, GameConstants.MaxAngle);
                var state = sim.CreateState(positions, _accelerometers, _anglometers);
                state.Tick = _game.Tick;
                state.CarStates[carIndex].TargetLaneIndex = _targetLane;

                // Add turbodata for simulator
                if (_turboData != null && _turboTicksRemaining > 0)
                {
                    state.CarStates[carIndex].TurboFactor = _turboData.TurboFactor;
                    state.CarStates[carIndex].TurboTicksRemaining = _turboTicksRemaining;
                }

                // Set my accelerations from prediction since currently can see only previous values
                if (_predictedState != null)
                {
                    state.CarStates[carIndex].Acceleration = _predictedState.CarStates[carIndex].Acceleration;
                    state.CarStates[carIndex].AngularAcceleration =
                        _predictedState.CarStates[carIndex].AngularAcceleration;
                }

#if LOG
                // Save the state for debugging
                _simStates.Add(state.Clone());
#endif

                // Make plans
                var nextPiece = _race.Track.GetNextPiece(pos.PiecePosition);

                int[] lanes = _race.Track.GetPossibleLaneIndexes(pos.PiecePosition.Lane.EndLaneIndex);
                var turboUses = new[] {false};
                var throttleTicks = new[] {0, 1};

                var plans = (
                    from l in lanes
                    from l2 in _race.Track.GetPossibleLaneIndexes(l)
                    from tb in turboUses
                    from tt in throttleTicks
                    select new Plan(tb, tt, l, l2)
                    ).ToList();

                // Get best plan
                var plan = ChoosePlan(sim, state, plans);
                _targetLane = plan.TargetLane;

                // Lane switching
                if (_targetLane != pos.PiecePosition.Lane.EndLaneIndex)
                {
                    if (nextPiece.Switch)
                    {
                        var tmpState = state.Clone();
                        sim.Advance(ref tmpState);

                        if (tmpState.CarStates[carIndex].PiecePosition.PieceIndex != pos.PiecePosition.PieceIndex)
                        {
                            // Will be on next piece on next tick 
                            // --> Do the switch now

                            if (_targetLane < pos.PiecePosition.Lane.EndLaneIndex)
                            {
                                Console.WriteLine("Switching to left ({0} to {1})", pos.PiecePosition.Lane.EndLaneIndex,
                                    _targetLane);
                                _game.LaneLeft();
                                return;
                            }
                            else if (_targetLane > pos.PiecePosition.Lane.EndLaneIndex)
                            {
                                Console.WriteLine("Switching to right ({0} to {1})", pos.PiecePosition.Lane.EndLaneIndex,
                                    _targetLane);
                                _game.LaneRight();
                                return;
                            }
                        }
                    }
                }

                Throttle = plan.ThrottleTicks == 0 ? 0.0 : 1.0;

                /*
            if (plan.UseTurbo && _turboAvailable && _turboData != null)
            {
                Console.WriteLine("Using turbo");
                _game.Turbo("Turbo!");
                _turboAvailable = false;
                _turboTicksRemaining = _turboData.TurboDurationTicks;
                return;
            }
            */

                // The using turbo based on plans doesn''t really work. So do the old way.
                if (_turboAvailable && _turboData != null)
                {
                    double len;
                    state.CarStates[carIndex].Throttle = 1.0;
                    state.CarStates[carIndex].TurboFactor = _turboData.TurboFactor;
                    state.CarStates[carIndex].TurboTicksRemaining = _turboData.TurboDurationTicks;

                    if (sim.GetCrashPoint(ref state, carIndex, 30, out len) == -1 &&
                        Math.Abs(pos.Angle) < 55)
                    {
                        Console.WriteLine("Using turbo");
                        _game.Turbo("Turbo!");
                        _turboAvailable = false;
                        _turboTicksRemaining = _turboData.TurboDurationTicks;
                        return;
                    }
                }

                _game.Throttle(Throttle);

                try
                {
                    // Create a predicted state for the next tick
                    _predictedState = sim.CreateState(positions, _accelerometers, _anglometers);
                    if (_turboData != null && _turboTicksRemaining > 0)
                    {
                        _predictedState.CarStates[carIndex].TurboFactor = _turboData.TurboFactor;
                        _predictedState.CarStates[carIndex].TurboTicksRemaining = _turboTicksRemaining;
                    }
                    _predictedState.CarStates[carIndex].Throttle = Throttle;
                    _predictedState.CarStates[carIndex].TargetLaneIndex = _targetLane;
                    sim.Advance(ref _predictedState);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error saving predicted state throttle\n" + ex.ToString());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error in carPositions\n" + ex.ToString());

                if (reply)
                {
                    _game.Ping();
                }
            }
        }

        public void TurboAvailable(TurboData tb)
        {
            _turboAvailable = true;
            _turboData = tb;
        }

        public void TurboStart(CarIdentifier carIdentifier)
        {
            
        }

        public void TurboEnd(CarIdentifier carIdentifier)
        {

        }

        public void GameEnd()
        {
            _game.Ping();
        }

        public void Crash(CarIdentifier identifier)
        {
            Save();

            Console.WriteLine(identifier.Name + "Crashed");

            if (identifier.Color.Equals(_myCar.Color))
            {
                Console.WriteLine("**********************************************");

                GameConstants.GravitySamples = GameConstants.GravitySamples.Select(g => g * 0.9).ToList();
                GameConstants.Gravity *= 0.90;


                Console.WriteLine("Lowered gravity to: " + GameConstants.Gravity);
                if (GameConstants.MaxAngle > 45)
                {
                    GameConstants.MaxAngle *= 0.975;
                    Console.WriteLine("Lowered max angle to: " + GameConstants.MaxAngle);
                }

                _maxAngleForLap = 99;
                // _isCrashed = true;
            }
        }

        public void Spawn(CarIdentifier identifier)
        {
            if (identifier.Color.Equals(_myCar.Color))
            {
                // _isCrashed = false;
            }
        }

        public void LapFinished(LapFinishedData data)
        {
            if (data.Car.Color.Equals(_myCar.Color))
            {
                Console.WriteLine("Finished lap {0} in {1} ms", data.LapTime.Lap, data.LapTime.Millis);
                Console.WriteLine("Max angle for lap was " + _maxAngleForLap);

                if (_maxAngleForLap < 40)
                {
                    GameConstants.Gravity *= 1.1;
                    Console.WriteLine("Upped gravity to " + GameConstants.Gravity);
                }
                else if (_maxAngleForLap > 55 || _maxAngleForLap < 60)
                {
                    GameConstants.Gravity *= 0.95;
                    Console.WriteLine("Dropped gravity to " + GameConstants.Gravity);
                }


                _maxAngleForLap = 0;
            }
        }

        public void Disqualified(CarIdentifier identifier)
        {

        }

        #region Speed things

        /*
        private double ThrottleForTarget(double target)
        {
            // Throttle = (v1 - v0 + 0.02 * v) / 0.2
            var v = _accelerometer.Velocity;

            var ret = (target - v + GameConstants.Friction * v) / GameConstants.Acceleration;
            if (ret < 0)
            {
                return 0;
            }
            else if (ret > 1)
            {
                return 1.0;
            }
            return ret;
        }
        */

        #endregion

        #region Metering

        private void InitializeMeters()
        {
            _turboAvailable = false;
            _turboTicksRemaining = 0;

            _accelerometers = new Dictionary<string, Accelerometer>();
            _anglometers = new Dictionary<string, Accelerometer>();
            _prevPositions = new Dictionary<string, CarPosition>();
            _traveledDistance = new Dictionary<string, double>();

            foreach (var car in _race.Cars)
            {
                _accelerometers.Add(car.Id.Color, new Accelerometer());
                _anglometers.Add(car.Id.Color, new Accelerometer());

                _prevPositions.Add(car.Id.Color, null);
                _traveledDistance.Add(car.Id.Color, 0);
            }

            Console.WriteLine("Initialized meters. (" + _accelerometers.Count + ")");
        }

        protected void UpdateMeters(IEnumerable<CarPosition> positions)
        {
            foreach (var pos in positions)
            {
                try
                {
                    CarPosition prevPos;

                    if (!_prevPositions.TryGetValue(pos.Id.Color, out prevPos))
                    {
                        prevPos = null;
                    }

                    bool newPiece = (prevPos != null && pos.PiecePosition.PieceIndex != prevPos.PiecePosition.PieceIndex);

                    if (newPiece)
                    {
                        var prevPiece = _race.Track.GetPreviousPiece(pos.PiecePosition);

                        if (prevPos.PiecePosition.Lane.StartLaneIndex != prevPos.PiecePosition.Lane.EndLaneIndex)
                        {
                            _traveledDistance[pos.Id.Color] = _traveledDistance[pos.Id.Color] +
                                                              prevPiece.GetSwitchingLength(
                                                                  _race.Track.Lanes[
                                                                      prevPos.PiecePosition.Lane.StartLaneIndex]
                                                                      .DistanceFromCenter,
                                                                  _race.Track.Lanes[
                                                                      prevPos.PiecePosition.Lane.EndLaneIndex]
                                                                      .DistanceFromCenter
                                                                  );
                        }
                        else
                        {
                            _traveledDistance[pos.Id.Color] = _traveledDistance[pos.Id.Color] + prevPiece.GetLength(_race.Track.GetLanePosition(pos.PiecePosition.Lane.StartLaneIndex));
                        }
                    }

                    _accelerometers[pos.Id.Color].SetPosition(_game.Tick,
                        _traveledDistance[pos.Id.Color] + pos.PiecePosition.InPieceDistance);
                    _anglometers[pos.Id.Color].SetPosition(_game.Tick, pos.Angle);
                    _prevPositions[pos.Id.Color] = pos;
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error updating meters (" + pos.Id.Color + ") " + ex.ToString());
                }
            }
        }

        private class DetectorData
        {
            public double angle;
            public double sign;
            public double v;
            public double av;
            public double aa;
            public double radius;

            public DetectorData(double angle, double sign, double v, double av, double aa, double radius)
            {
                this.angle = angle;
                this.sign = sign;
                this.v = v;
                this.av = av;
                this.aa = aa;
                this.radius = radius;
            }
        }

        private DetectorData _prevDetectorData2;
        private DetectorData _prevDetectorData;

        private void DetectConstants(CarPosition prevPos, CarPosition pos)
        {
            if (GameConstants.GravitySamples.Count >= 20)
            {
                return;
            }

            if (_game.Tick == 1)
            {
                // Detect acceleration
                GameConstants.Acceleration = _accelerometers[_myCar.Color].Velocity;
                Console.WriteLine("Detected acceleration = " + GameConstants.Acceleration);
                Debug.WriteLine("Detected acceleration = " + GameConstants.Acceleration);
            }
            else if (_game.Tick == 2)
            {
                // Detect friction
                GameConstants.Friction = (GameConstants.Acceleration - _accelerometers[_myCar.Color].Acceleration) /
                                         GameConstants.Acceleration;
                Console.WriteLine("Detected friction = " + GameConstants.Friction);
                Debug.WriteLine("Detected friction = " + GameConstants.Friction);
            }
            else if (prevPos != null && GameConstants.GravitySamples.Count < 20)
            {
                // Detect gravity
                var piece = _race.Track.GetPiece(pos.PiecePosition);
                var accelerometer = _accelerometers[pos.Id.Color];
                var anglometer = _anglometers[pos.Id.Color];

                if (!piece.Angle.HasValue || piece.Switch)
                {
                    _prevDetectorData = null;
                    _prevDetectorData2 = null;
                    return;
                }

                var dd = new DetectorData(pos.Angle, Math.Sign(pos.Angle), accelerometer.Velocity, anglometer.Velocity, anglometer.Acceleration, piece.GetRadius(_race.Track.Lanes[pos.PiecePosition.Lane.StartLaneIndex]));

                if (!piece.Switch && piece.Radius.HasValue
                    && Math.Abs(dd.angle) > 3 && Math.Abs(dd.angle) < 45
                    && Math.Abs(dd.aa) > 0.1
                    && _prevDetectorData != null
                    && _prevDetectorData2 != null)
                {
                    var angle = _prevDetectorData2.angle;
                    var sign = _prevDetectorData2.sign;
                    var v = _prevDetectorData.v;
                    var av = _prevDetectorData.av;
                    var aa = dd.aa;

                    var restoringForce = v * GameConstants.SpringVelocityFactor * (-av - angle) -
                                         GameConstants.SpringAvFactor * av;
                    var forceFromOverSpeed = aa - restoringForce;

                    if (Math.Abs(forceFromOverSpeed) > 0.5)
                    {
                        var overspeed = (-GameConstants.VelocityFactor +
                                         Math.Sqrt(Math.Pow(GameConstants.VelocityFactor, 2) +
                                                   4 * GameConstants.Velocity2Factor * sign * forceFromOverSpeed))
                                        / (2 * GameConstants.Velocity2Factor);

                        if (double.IsNaN(overspeed) || overspeed < 0)
                        {
                            overspeed = (-GameConstants.VelocityFactor -
                                         Math.Sqrt(Math.Pow(GameConstants.VelocityFactor, 2) +
                                                   4 * GameConstants.Velocity2Factor * sign * forceFromOverSpeed))
                                        / (2 * GameConstants.Velocity2Factor);
                        }

                        if (!double.IsNaN(overspeed) && overspeed > 0)
                        {
                            var vmax = v - overspeed;

                            var gravity = Math.Pow(vmax, 2) /
                                          (GameConstants.Friction *
                                           dd.radius);

                            if (!double.IsNaN(gravity) && !double.IsInfinity(gravity))
                            {
                                GameConstants.GravitySamples.Add(gravity);
                                GameConstants.Gravity = GetGoodAverage(GameConstants.GravitySamples) * 0.95;

                                if (GameConstants.GravitySamples.Count == 20)
                                {
                                    Console.WriteLine("Detected gravity = " + GameConstants.Gravity);
                                }
                                else
                                {
                                    Console.WriteLine("Early gravity = " + gravity);
                                }

                                // This will make next sample to happen in next 3rd tick
                                _prevDetectorData = null;
                                _prevDetectorData2 = null;
                            }
                        }
                        else
                        {
                            // Console.WriteLine("Overspeed was " + overspeed);
                            // return; // Not good
                        }
                    }
                }

                _prevDetectorData2 = _prevDetectorData;
                _prevDetectorData = dd;
            }
        }

        private static double GetGoodAverage(List<double> values)
        {
            if (values.Count == 1) return values.First();

            double avg = values.Average();
            double sum = values.Sum(d => (d - avg) * (d - avg));
            double stddev = Math.Sqrt(sum / (double)values.Count);

            // Skip values that are more than 2 stddevs away
            return values.Where(v => Math.Abs(v - avg) < 2 * stddev).Average();
        }

        #endregion

        #region Plan testing

        private Plan ChoosePlan(Simulator sim, GameState state, List<Plan> plans)
        {
            foreach (var plan in plans)
            {
                TestPlan(sim, state, plan);
            }

            var ret = plans
                .OrderByDescending(p => p.TicksTraveled)
                .ThenByDescending(p => p.Distance)
                .First();

            // If the top plan has less than 50 ticks -> choose a plan with no throttle ticks.
            if (ret.TicksTraveled < 50)
            {
                return plans
                    .Where(p => p.ThrottleTicks == 0)
                    .OrderByDescending(p => p.TicksTraveled)
                    .ThenByDescending(p => p.Distance)
                    .First();
            }

            return ret;
        }

        private void TestPlan(Simulator sim, GameState pState, Plan plan)
        {
            const int ticksToLookForward = 250;
            var carIndex = _race.Cars.FindIndex(c => c.Id.Color.Equals(_myCar.Color));
            var state = pState.Clone();

            state.CarStates[carIndex].TargetLaneIndex = plan.TargetLane;
            state.CarStates[carIndex].TargetLaneIndex2 = plan.TargetLane2;

            for (int i = 0; i < state.CarStates.Length; i++)
            {
                if (i != carIndex)
                {
                    state.CarStates[i].Throttle = 0;
                }
            }

            // Use turbo
            if (plan.UseTurbo && _turboData != null)
            {
                state.CarStates[carIndex].TurboFactor = _turboData.TurboFactor;
                state.CarStates[carIndex].TurboTicksRemaining = _turboData.TurboDurationTicks;
            }

            // Throttle for specified amount of ticks
            state.CarStates[carIndex].Throttle = 1;
            for (int i = 0; i < plan.ThrottleTicks; i++)
            {
                sim.Advance(ref state);

                if (state.CarStates[carIndex].IsCrashed)
                {
                    break;
                }

                plan.TicksTraveled++;
            }

            // Decelerate for the rest of the time
            state.CarStates[carIndex].Throttle = 0;
            for (int i = 0; i < ticksToLookForward - plan.ThrottleTicks; i++)
            {
                if (i > 50)
                {
                    KeepMinSpeed(state, 2.0);
                }

                sim.Advance(ref state);

                if (state.CarStates[carIndex].IsCrashed)
                {
                    break;
                }

                plan.TicksTraveled++;
            }

            plan.Distance = state.CarStates[carIndex].TraveledMidLaneDistance;
        }

        private static void KeepMinSpeed(GameState state, double minSpeed)
        {
            foreach (CarState t in state.CarStates)
            {
                t.Throttle = t.Velocity < minSpeed ? 1.0 : 0.0;
            }
        }

        #endregion
    }

    public class ConstantThrottleAi : IAi
    {
        private readonly IGame _game;
        private readonly double _throttle;

        public ConstantThrottleAi(IGame game, double throttle)
        {
            _game = game;
            _throttle = throttle;
        }

        public void Joined(string name, string key) { }
        public void YourCar(CarIdentifier identifier) { }
        public void GameInit(Race race) { }

        public void GameStart()
        {
            _game.Ping();
        }

        public void CarPositions(List<CarPosition> positions, bool reply)
        {
            if (reply)
            {
                _game.Throttle(_throttle);
            }
        }

        public void GameEnd() { }
        public void Crash(CarIdentifier identifier) { }
        public void Spawn(CarIdentifier identifier) { }
        public void Disqualified(CarIdentifier identifier) { }
        public void TurboAvailable(TurboData tb) { }
        public void TurboStart(CarIdentifier carIdentifier) { }
        public void TurboEnd(CarIdentifier carIdentifier) { }
        public void LapFinished(LapFinishedData data) { }
    }

    public class ConstantVelocityAi : Ai
    {
        private readonly double _targetVelocity;

        public ConstantVelocityAi(IGame game, double targetVelocity) : base(game)
        {
            _targetVelocity = targetVelocity;
        }

        public override void CarPositions(List<CarPosition> positions, bool reply)
        {
            if (_race == null || _race.Track == null || _race.Track.Lanes == null) return;

            var carIndex = _race.Cars.FindIndex(c => c.Id.Color.Equals(_myCar.Color));
            var pos = positions.First(cp => cp.Id.Color == _myCar.Color);
            var prevPos = _prevPositions[_myCar.Color];

            // Update meters
            UpdateMeters(positions);

            if (!reply) return;

            // Run the constant detection
            // DetectConstants(prevPos, pos);

            var v = _accelerometers[_myCar.Color].Velocity;
            Throttle = ThrottleForTarget(v, _targetVelocity);
            _game.Throttle(Throttle);
        }

        private double ThrottleForTarget(double current, double target)
        {
            // Throttle = (v1 - v0 + 0.02 * v) / 0.2
            var v = current;

            var ret = (target - v + GameConstants.Friction * v) / GameConstants.Acceleration;
            if (ret < 0)
            {
                return 0;
            }
            else if (ret > 1)
            {
                return 1.0;
            }
            return ret;
        }
    }
}
