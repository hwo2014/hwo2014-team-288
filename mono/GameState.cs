﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CarBot
{
    public class CarState
    {
        public double TraveledMidLaneDistance;
        public double TraveledDistance;
        public double Throttle;
        public double Velocity;
        public double Acceleration;
        public double Angle;
        public int TargetLaneIndex;
        public int TargetLaneIndex2;
        public bool IsCrashed;

        public PiecePosition PiecePosition;
        public double AngularVelocity;
        public double AngularAcceleration;
        public int TurboTicksRemaining;
        public double TurboFactor;

        public CarState Clone()
        {
            var ret = (CarState)MemberwiseClone();
            ret.PiecePosition = PiecePosition.Clone();
            return ret;
        }
    }

    public class GameState
    {
        public int Tick;
        public CarState[] CarStates;

        public GameState Clone()
        {
            var ret = new GameState();
            ret.Tick = Tick;

            if (CarStates != null)
            {
                ret.CarStates = new CarState[CarStates.GetLength(0)];

                for (int i = 0; i < CarStates.GetLength(0); i++)
                {
                    ret.CarStates[i] = CarStates[i].Clone();
                }
            }

            return ret;
        }
    }
}
