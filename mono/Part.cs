﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CarBot
{
    public class Part
    {
        public double Length;
        public double MaxSpeed;
        public bool CanSwitch;
        public Piece Piece;

        public Part(double length, double maxSpeed, bool canSwitch, Piece piece)
        {
            if (Length  < 0) throw new ArgumentOutOfRangeException("length", "Length cannot be negative");

            Length = length;
            MaxSpeed = maxSpeed;
            CanSwitch = canSwitch;
            Piece = piece;
        }

        public double GetSwitchingLength(double fromLanePos, double toLanePos)
        {
            return Piece.GetSwitchingLength(fromLanePos, toLanePos);
        }
    }
}
