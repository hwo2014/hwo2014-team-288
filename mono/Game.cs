using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CarBot
{
    public class JoinResponse
    {
        public string Name { get; set; }
        public string Key { get; set; }
    }

    public class Piece
    {
        public double? Length { get; set; }
        public double? Radius { get; set; }
        public double? Angle { get; set; }
        public double? RealStartAngle { get; set; }
        public bool Switch { get; set; }

        public double GetLength(double lanePosition)
        {
            if (Length.HasValue)
            {
                return Length.Value;
            }

            double r = Radius.Value + (Angle < 0 ? lanePosition : -lanePosition);
            return r * Math.PI * (Math.Abs(Angle.Value) / 180.0);
        }

        public double GetRadius(Lane lane)
        {
            return Radius.Value + (Angle < 0 ? lane.DistanceFromCenter : -lane.DistanceFromCenter);
        }

        public double GetMaxSpeed(double lanePosition)
        {
            if (!Length.HasValue)
            {
                Debug.Assert(Radius != null, "Radius != null");

                var r = Radius.Value + (Angle < 0 ? lanePosition : -lanePosition);
                // var v = Math.Sqrt(GameConstants.Ug * r) + GameConstants.MaxAngle / GameConstants.MaxAngleSlope;
                var v = Math.Sqrt(GameConstants.Ug * r);
                return v;
            }

            return 999999; // No limit for straight pieces
        }

        public double GetSwitchingLength(double fromLanePos, double toLanePos)
        {
            Debug.Assert(Switch);

            if (Length.HasValue)
            {
                return Math.Sqrt(
                    Math.Pow(fromLanePos - toLanePos, 2) +
                    Math.Pow(Length.Value, 2)) * 1.0007833341; // Magic constant to fix length
            }
            else
            {
                Debug.Assert(Radius != null, "Radius != null");
                Debug.Assert(Angle != null, "Angle != null");

                double r1 = Radius.Value + (Angle < 0 ? fromLanePos : -fromLanePos);
                double r2 = Radius.Value + (Angle < 0 ? toLanePos : -toLanePos);

                return Math.Sqrt(
                    Math.Pow(r1, 2) +
                    Math.Pow(r2, 2) -
                    2 * r1 * r2 * Math.Cos(MathTools.DegToRad(Angle.Value))
                    ) * 1.0073438439; // magic fixing constant
            }
        }

        public override string ToString()
        {
            return string.Format("Length: {0}, Radius: {1}, Angle: {2}, Switch: {3}", Length, Radius, Angle, Switch);
        }
    }

    public class Lane
    {
        public int Index { get; set; }
        public double DistanceFromCenter { get; set; }
    }

    public class Track
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public List<Piece> Pieces { get; set; }
        public List<Lane> Lanes { get; set; }

        public Track()
        {
            Pieces = new List<Piece>();
        }

        public Piece GetPiece(PiecePosition piecePosition)
        {
            return Pieces[piecePosition.PieceIndex];
        }

        public Piece GetPreviousPiece(PiecePosition piecePosition)
        {
            int index = piecePosition.PieceIndex - 1;
            if (index == -1)
            {
                index = Pieces.Count - 1;
            }

            return Pieces[index];
        }

        public Piece GetNextPiece(PiecePosition piecePosition)
        {
            return Pieces[(piecePosition.PieceIndex + 1) % Pieces.Count];
        }

        /// <summary>
        /// Gets the piece length for the specified car position. 
        /// Returns switching length for switching positions.
        /// </summary>
        public double GetPieceLength(PiecePosition pp)
        {
            var piece = GetPiece(pp);

            if (pp.Lane.StartLaneIndex != pp.Lane.EndLaneIndex)
            {
                return piece.GetSwitchingLength(
                    GetLanePosition(pp.Lane.StartLaneIndex),
                    GetLanePosition(pp.Lane.EndLaneIndex));
            }

            return piece.GetLength(GetLanePosition(pp.Lane.StartLaneIndex));
        }

        public double GetLanePosition(int laneIndex)
        {
            return Lanes[laneIndex].DistanceFromCenter;
        }

        public int[] GetPossibleLaneIndexes(int laneIndex)
        {
            int leftIndex = Math.Max(laneIndex - 1, 0);
            int rightIndex = Math.Min(laneIndex + 1, Lanes.Count - 1);
            return Enumerable.Range(leftIndex, rightIndex - leftIndex + 1).ToArray();
        }
    }

    public class Race
    {
        public Track Track { get; set; }
        public List<Car> Cars { get; set; }
        public RaceSession RaceSession { get; set; }

        public Race()
        {
            Cars = new List<Car>();
        }

        public int GetCarIndex(string color)
        {
            return Cars.FindIndex(c => c.Id.Color.Equals(color));
        }

        public int GetCarIndex(CarIdentifier car)
        {
            return Cars.FindIndex(c => c.Id.Color.Equals(car.Color));
        }
    }

    public class GameInit
    {
        public Race Race { get; set; }
    }

    public class CarIdentifier
    {
        public string Name { get; set; }
        public string Color { get; set; }
    }

    public class DisqualifiedResponse
    {
        public CarIdentifier Car;
        public string Reason;
    }

    public class Dimensions
    {
        public double Length { get; set; }
        public double Width { get; set; }
        public double GuideFlagPosition { get; set; }
    }

    public class Car
    {
        public CarIdentifier Id { get; set; }
        public Dimensions Dimensions { get; set; }
    }

    public class RaceSession
    {
        public int Laps { get; set; }
        public int MaxLapTimeMs { get; set; }
        public bool QuickRace { get; set; }
        public int DurationMs { get; set; }
    }

    public struct PiecePositionLane
    {
        public int StartLaneIndex { get; set; }
        public int EndLaneIndex { get; set; }

        public PiecePositionLane(int startLaneIndex, int endLaneIndex) : this()
        {
            StartLaneIndex = startLaneIndex;
            EndLaneIndex = endLaneIndex;
        }

        public override string ToString()
        {
            return string.Format("Lane: {0} --> {1}", StartLaneIndex, EndLaneIndex);
        }
    }

    public class PiecePosition
    {
        public int PieceIndex { get; set; }
        public double InPieceDistance { get; set; }
        public PiecePositionLane Lane { get; set; }
        public int Lap { get; set; }

        public PiecePosition Clone()
        {
            return (PiecePosition)MemberwiseClone();
        }

        public override string ToString()
        {
            return string.Format("Lap: {0} piece: {1} d: {2} Lane: {3}", Lap, PieceIndex, InPieceDistance, Lane);
        }
    }

    public class CarPosition
    {
        public CarIdentifier Id { get; set; }
        public double Angle { get; set; }
        public double? AngleOffset { get; set; }
        public PiecePosition PiecePosition { get; set; }
    }

    public class TurboData
    {
        public double TurboDurationMilliseconds { get; set; }
        public int TurboDurationTicks { get; set; }
        public double TurboFactor { get; set; }
    }

    public class LapTime
    {
        public int Lap { get; set; }
        public int Ticks { get; set; }
        public int Millis { get; set; }
    }

    public class RaceTime
    {
        public int Laps { get; set; }
        public int Ticks { get; set; }
        public int Millis { get; set; }
    }

    public class Ranking
    {
        public int Overall { get; set; }
        public int FastestLap { get; set; }
    }

    public class LapFinishedData
    {
        public CarIdentifier Car { get; set; }
        public LapTime LapTime { get; set; }
        public RaceTime RaceTime { get; set; }
        public Ranking Ranking { get; set; }
    }

    public interface IGame
    {
        int Tick { get; }
        void Throttle(double value);
        void Ping();
        void LaneLeft();
        void LaneRight();
        void Turbo(string msg);
    }

    public interface IAi
    {
        void Joined(string name, string key);
        void YourCar(CarIdentifier identifier);
        void GameInit(Race race);
        void GameStart();
        void CarPositions(List<CarPosition> positions, bool reply);
        void GameEnd();
        void Crash(CarIdentifier identifier);
        void Spawn(CarIdentifier identifier);
        void Disqualified(CarIdentifier identifier);
        void TurboAvailable(TurboData tb);
        void TurboStart(CarIdentifier carIdentifier);
        void TurboEnd(CarIdentifier carIdentifier);
        void LapFinished(LapFinishedData data);
    }

    class Game : IGame
    {
        private readonly string _host;
        private readonly int _port;
        private readonly string _botName;
        private readonly string _botKey;

        private StreamWriter _writer;
        private string _color;
        private string _password;
        private int _carCount;

#if LOG
        private StreamWriter _inputLogger;
        private StreamWriter _outputLogger;
#endif
        public enum GameMode { Join, JoinRace, CreateRace }
        private GameMode _gameMode;

        #region Constructors

        public Game(string host, int port, string botName, string botKey, string color, string password, int carCount, GameMode mode)
        {
            _host = host;
            _port = port;
            _botName = botName;
            _botKey = botKey;
            _color = color;
            _password = password;
            _carCount = carCount;
            _gameMode = mode;
        }

        #endregion

        #region Properties

        public int Tick { get; private set; }

        #endregion

        public void Run(IAi ai)
        {
#if LOG
            if (_gameMode == GameMode.CreateRace || _gameMode == GameMode.Join)
            {
                var ts = DateTime.Now.ToString("s").Replace(':', '-');

                _inputLogger =
                    new StreamWriter(new FileStream(Path.Combine(@"c:\tmp\car\", ts + "_" + _gameMode + "_in.txt"),
                        FileMode.CreateNew, FileAccess.Write, FileShare.Read));
                _outputLogger =
                    new StreamWriter(new FileStream(Path.Combine(@"c:\tmp\car\", ts + "_" + _gameMode + "_out.txt"),
                        FileMode.CreateNew, FileAccess.Write, FileShare.Read));

                _inputLogger.AutoFlush = true;
                _outputLogger.AutoFlush = true;
            }
#endif
            using (var client = new TcpClient(_host, _port))
            {
                var stream = client.GetStream();
                var reader = new StreamReader(stream);
                _writer = new StreamWriter(stream) { AutoFlush = true };

                if (_gameMode == GameMode.Join)
                {
                    Send(new Join(_botName, _botKey, _color));
                }
                else if (_gameMode == GameMode.JoinRace)
                {
                    Send(new JoinRace(_botName, _botKey, _password, _carCount));
                }
                else if (_gameMode == GameMode.CreateRace)
                {
                    // Send(new CreateRace("usa", _password, 1, _botName, _botKey, 5));
                    // Send(new CreateRace("germany", _password, 1, _botName, _botKey, 5));
                    // Send(new CreateRace("france", _password, _carCount, _botName, _botKey, 5));
                    // Send(new CreateRace("keimola", _password, _carCount, _botName, _botKey, 5));

                    Send(new CreateRace("elaeintarha", _password, _carCount, _botName, _botKey, 5));
                    // Send(new CreateRace("imola", _password, _carCount, _botName, _botKey, 5));
                    // Send(new CreateRace("england", _password, _carCount, _botName, _botKey, 5));
                    // Send(new CreateRace("suzuka", _password, _carCount, _botName, _botKey, 5));
                }

                string line;
                while ((line = reader.ReadLine()) != null)
                {
#if LOG
                    if (_inputLogger != null)
                    {
                        _inputLogger.WriteLine(line);

                        if (Tick % 100 == 0) Console.WriteLine("T" + Tick);
                    }
#endif

                    var msg = JsonConvert.DeserializeObject<InMsgWrapper>(line);

                    if (msg.gameTick.HasValue)
                    {
                        Tick = msg.gameTick.Value;
                    }

                    switch (msg.msgType)
                    {
                        case "carPositions":
                            var cps = ((JArray)msg.data).ToObject<List<CarPosition>>();
                            ai.CarPositions(cps, msg.gameTick.HasValue);
                            break;
                        case "yourCar":
                            Console.WriteLine("Your car");
                            var yc = ((JObject)msg.data).ToObject<CarIdentifier>();
                            ai.YourCar(yc);
                            break;
                        case "join":
                            Console.WriteLine("Joined");
                            var j = ((JObject)msg.data).ToObject<JoinResponse>();
                            ai.Joined(j.Name, j.Key);
                            break;
                        case "gameInit":
                            Console.WriteLine("Race init");
                            var gi = ((JObject)msg.data).ToObject<GameInit>();
                            ai.GameInit(gi.Race);
                            break;
                        case "gameEnd":
                            Console.WriteLine("Race ended");
                            ai.GameEnd();
                            break;
                        case "gameStart":
                            Console.WriteLine("Race starts");
                            ai.GameStart();
                            break;
                        case "crash":
                            var ci = ((JObject)msg.data).ToObject<CarIdentifier>();
                            ai.Crash(ci);
                            break;
                        case "spawn":
                            var ci2 = ((JObject)msg.data).ToObject<CarIdentifier>();
                            ai.Spawn(ci2);
                            break;
                        case "dnf":
                            var dq = ((JObject)msg.data).ToObject<DisqualifiedResponse>();
                            Console.WriteLine("Disqualified " + dq.Car.Name + " " + dq.Reason);
                            ai.Disqualified(dq.Car);
                            break;
                        case "turboAvailable":
                            Console.WriteLine("Turbo available");
                            var tb = ((JObject)msg.data).ToObject<TurboData>();
                            ai.TurboAvailable(tb);
                            break;
                        case "turboStart":
                            var ci4 = ((JObject)msg.data).ToObject<CarIdentifier>();
                            ai.TurboStart(ci4);
                            break;
                        case "turboEnd":
                            var ci5 = ((JObject)msg.data).ToObject<CarIdentifier>();
                            ai.TurboEnd(ci5);
                            break;
                        case "lapFinished":
                            var lf = ((JObject)msg.data).ToObject<LapFinishedData>();
                            ai.LapFinished(lf);
                            break;
                        case "finish":
                            break;
                        case "tournamentEnd":
                            break;
                        case "error":
                            Console.WriteLine("Error from server" + line);
                            break;
                        default:
                            Console.WriteLine("Unexpected msg " + msg.msgType + "\n" + line);
                            // Send(new Ping());
                            break;
                    }
                }

                try
                {
                    reader.Close();
                    _writer.Close();
                    stream.Close();
                    _writer = null;
                }
                catch (Exception) { }

#if LOG
                if (_inputLogger != null) _inputLogger.Close();
                if (_outputLogger != null) _outputLogger.Close();
#endif
            }            
        }

        public void Throttle(double value)
        {
            Send(new Throttle(value));
        }

        public void Ping()
        {
            Send(new Ping());
        }

        public void LaneLeft()
        {
            Send(new SwitchLane("Left"));
        }

        public void LaneRight()
        {
            Send(new SwitchLane("Right"));
        }

        public void Turbo(string msg)
        {
            Send(new Turbo(msg));
        }

        private void Send(SendMsg msg)
        {
            msg.gameTick = Tick;
#if LOG
            if (_inputLogger != null)
            {
                _outputLogger.WriteLine(msg.ToJson());
            }
#endif
            _writer.WriteLine(msg.ToJson());
        }
    }

    public class InMsgWrapper
    {
        public string msgType;
        public object data;
        public string gameId;
        public int? gameTick;

        public InMsgWrapper(string msgType, object data, string gameId, int? gameTick)
        {
            this.msgType = msgType;
            this.data = data;
            this.gameId = gameId;
            this.gameTick = gameTick;
        }
    }

    public class MsgWrapper
    {
        public string msgType;
        public object data;
        public int gameTick;

        public MsgWrapper(string msgType, object data, int gameTick)
        {
            this.msgType = msgType;
            this.data = data;
            this.gameTick = gameTick;
        }
    }

    public abstract class SendMsg
    {
        public int gameTick;
        
        public string ToJson()
        {
            return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData(), gameTick));
        }

        protected virtual Object MsgData()
        {
            return this;
        }

        protected abstract string MsgType();
    }

    public class Join : SendMsg
    {
        public string name;
        public string key;
        public string color;

        public Join(string name, string key, string color)
        {
            this.name = name;
            this.key = key;
            this.color = color;
        }

        protected override string MsgType()
        {
            return "join";
        }
    }

    public class JoinRace : SendMsg
    {
        public BotIdentifier botId;
        public string password;
        public int carCount;

        public JoinRace(string name, string key, string password, int carCount)
        {
            this.password = password;
            this.carCount = carCount;
            this.botId = new BotIdentifier(name, key);
        }

        protected override string MsgType()
        {
            return "joinRace";
        }
    }

    public class BotIdentifier
    {
        public string name;
        public string key;

        public BotIdentifier(string name, string key)
        {
            this.name = name;
            this.key = key;
        }
    }

    public class CreateRace : SendMsg
    {
        public string trackName;
        public string password;
        public int carCount;
        public int laps;
        public BotIdentifier botId;

        public CreateRace(string trackName, string password, int carCount, string name, string key, int laps)
        {
            this.trackName = trackName;
            this.password = password;
            this.carCount = carCount;
            this.botId = new BotIdentifier(name, key);
            this.laps = laps;
        }

        protected override string MsgType()
        {
            return "createRace";
        }
    }

    public class Ping : SendMsg
    {
        protected override string MsgType()
        {
            return "ping";
        }
    }

    public class Throttle : SendMsg
    {
        public double value;

        public Throttle(double value)
        {
            this.value = value;
        }

        protected override Object MsgData()
        {
            return this.value;
        }

        protected override string MsgType()
        {
            return "throttle";
        }
    }

    public class Turbo : SendMsg
    {
        public string message;

        public Turbo(string message)
        {
            this.message = message;
        }

        protected override Object MsgData()
        {
            return this.message;
        }

        protected override string MsgType()
        {
            return "turbo";
        }
    }

    public class SwitchLane : SendMsg
    {
        public string value;

        public SwitchLane(string value)
        {
            this.value = value;
        }

        protected override Object MsgData()
        {
            return this.value;
        }

        protected override string MsgType()
        {
            return "switchLane";
        }
    }
}