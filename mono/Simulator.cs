﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace CarBot
{
    public class Simulator
    {
        private readonly Race _race;
        private readonly Track _track;
        private readonly double _maxAngle;

        #region Constructors

        public Simulator(Race race, double maxAngle)
        {
            _race = race;
            _track = race.Track;
            _maxAngle = maxAngle;
        }

        #endregion

        public GameState CreateState(List<CarPosition> positions, 
            Dictionary<string, Accelerometer> accelerometers,
            Dictionary<string, Accelerometer> anglometers)
        {
            var state = new GameState();
            state.CarStates = new CarState[_race.Cars.Count];

            foreach (var pos in positions)
            {
                int carIndex = _race.GetCarIndex(pos.Id);

                var carState = new CarState()
                {
                    Angle = pos.Angle,
                    PiecePosition = pos.PiecePosition.Clone(),
                    TargetLaneIndex = pos.PiecePosition.Lane.EndLaneIndex
                };

                var accelerometer = accelerometers[pos.Id.Color];
                var anglometer = anglometers[pos.Id.Color];

                carState.Acceleration = accelerometer.Acceleration;
                carState.Velocity = accelerometer.Velocity;

                carState.AngularAcceleration = anglometer.Acceleration;
                carState.AngularVelocity = anglometer.Velocity;

                carState.Throttle = (carState.Acceleration + GameConstants.Friction * carState.Velocity) / GameConstants.Acceleration;
                carState.TargetLaneIndex = carState.PiecePosition.Lane.EndLaneIndex;
                state.CarStates[carIndex] = carState;
            }

            return state;
        }

        public GameState GetDefaultState()
        {
            var state = new GameState();
            state.CarStates = new CarState[_race.Cars.Count];
            int i = 0;

            foreach (var car in _race.Cars)
            {
                state.CarStates[i++] = new CarState()
                {
                    PiecePosition = new PiecePosition()
                    {
                        Lane = new PiecePositionLane()
                    }
                };
            }

            return state;
        }

        public int GetCrashPoint(ref GameState pState, int carIndex, int maxTicks, out double pos)
        {
            var state = pState.Clone();
            state.CarStates[carIndex].Throttle = 1;

            for (int i = 0; i < maxTicks; i++)
            {
                Advance(ref state);

                if (state.CarStates[carIndex].IsCrashed)
                {
                    pos = state.CarStates[carIndex].TraveledDistance;
                    return i;
                }
            }

            pos = state.CarStates[carIndex].TraveledDistance;
            return -1;
        }

        #region Simulation

        public void Advance(ref GameState state, int ticks)
        {
            for (int i = 0; i < ticks; i++)
            {
                Advance(ref state);
            }
        }

        public void Advance(ref GameState state)
        {
            foreach (var cs in state.CarStates)
            {
                if (cs.IsCrashed) continue;

                var piece = _track.GetPiece(cs.PiecePosition);

                var prevPp = cs.PiecePosition.Clone();
                var prevTrav = cs.TraveledDistance;
                var prevTravMid = cs.TraveledMidLaneDistance;

                // Calculate the force applied to the angle spring
                double springForce = 0;
                var pieceMaxSpeed = piece.GetMaxSpeed(_track.GetLanePosition(cs.PiecePosition.Lane.StartLaneIndex));

                if (pieceMaxSpeed < cs.Velocity)
                {
                    // Applied force to the angle restoring spring
                    if (piece.Radius != null && piece.Angle.HasValue)
                    {
                        var overSpeed = (cs.Velocity - pieceMaxSpeed);

                        springForce += GameConstants.Velocity2Factor * Math.Pow(overSpeed, 2) + GameConstants.VelocityFactor * overSpeed;
                        springForce *= Math.Sign(piece.Angle.Value);
                    }
                }

                // Acceleration
                cs.Acceleration = GameConstants.Acceleration * cs.Throttle;

                if (cs.TurboTicksRemaining-- > 0)
                {
                    cs.Acceleration *= cs.TurboFactor;
                }

                // Friction
                cs.Acceleration -= GameConstants.Friction * cs.Velocity;

                cs.Velocity += cs.Acceleration;
                AdvanceCarPosition(cs, cs.Velocity);

                // Restoring force
                springForce += cs.Velocity * GameConstants.SpringVelocityFactor * (-cs.AngularVelocity - cs.Angle) - GameConstants.SpringAvFactor * cs.AngularVelocity;

                cs.AngularAcceleration = springForce;
                cs.AngularVelocity += cs.AngularAcceleration;
                cs.Angle += cs.AngularVelocity;

                // Check for crash
                if (Math.Abs(cs.Angle) > _maxAngle)
                {
                    cs.IsCrashed = true;
                    cs.Acceleration = 0;
                    cs.Velocity = 0;
                    cs.AngularAcceleration = 0;
                    cs.AngularVelocity = 0;
                    cs.Angle = 0;
                    cs.PiecePosition = prevPp;
                    cs.TraveledDistance = prevTrav;
                    cs.TraveledMidLaneDistance = prevTravMid;
                }
            }

            // Detect car-car crashes
            for (int i = 0; i < state.CarStates.GetLength(0); i++)
            {
                var car1 = _race.Cars[i];
                var cs1 = state.CarStates[i];
                if (cs1.IsCrashed) continue;

                var frontPos1 = GetFrontPiecePos(car1, cs1.PiecePosition);
                var backPos1 = GetBackPiecePos(car1, cs1.PiecePosition);

                for (int j = i + 1; j < state.CarStates.GetLength(0); j++)
                {
                    var car2 = _race.Cars[j];
                    var cs2 = state.CarStates[j];
                    if (cs2.IsCrashed) continue;

                    if (cs1.PiecePosition.Lane.StartLaneIndex != cs2.PiecePosition.Lane.StartLaneIndex ||
                        cs1.PiecePosition.Lane.EndLaneIndex != cs2.PiecePosition.Lane.EndLaneIndex)
                    {
                        continue; // Not on the same lane.
                    }

                    var frontPos2 = GetFrontPiecePos(car2, cs2.PiecePosition);
                    var backPos2 = GetBackPiecePos(car2, cs2.PiecePosition);

                    // Check which one is in front
                    int ret = PosIntersect(frontPos1, backPos1, frontPos2, backPos2);
                    if (ret == 1)
                    {
                        // Console.WriteLine("Bumping: {0} --> {1}", j, i);

                        // var sum = cs1.Velocity + cs2.Velocity;
                        // cs1.Velocity = sum * 0.75;
                        // cs2.Velocity = sum * 0.25;

                        AdvanceCarPosition(cs1, cs2.Velocity);
                        var p = Math.Min(cs2.PiecePosition.InPieceDistance, 1 * Math.Abs(cs2.Velocity - cs1.Velocity));
                        cs2.PiecePosition.InPieceDistance -= p;
                        cs2.TraveledDistance -= Math.Abs(cs1.Velocity - cs2.Velocity);
                        cs2.TraveledMidLaneDistance -= Math.Abs(cs1.Velocity - cs2.Velocity); // (Should be scaled)
                        cs2.Velocity = cs1.Velocity;
                    }
                    else if (ret == 2)
                    {
                        // Console.WriteLine("Bumping: {0} --> {1}", i, j);

                        // var sum = cs1.Velocity + cs2.Velocity;
                        // cs1.Velocity = sum * 0.25;
                        // cs2.Velocity = sum * 0.75;

                        AdvanceCarPosition(cs2, cs1.Velocity);
                        var p = Math.Min(cs1.PiecePosition.InPieceDistance, 1 * Math.Abs(cs1.Velocity - cs2.Velocity));
                        cs1.PiecePosition.InPieceDistance -= p;
                        cs1.TraveledDistance -= Math.Abs(cs1.Velocity - cs2.Velocity);
                        cs1.TraveledMidLaneDistance -= Math.Abs(cs1.Velocity - cs2.Velocity); // (Should be scaled)
                        cs1.Velocity = cs2.Velocity;
                    }
                }
            }
        }

        private void AdvanceCarPosition(CarState cs, double velocity)
        {
            var piece = _track.GetPiece(cs.PiecePosition);

            cs.PiecePosition.InPieceDistance += velocity;
            cs.TraveledDistance += cs.Velocity;

            double pieceLength = _track.GetPieceLength(cs.PiecePosition);
            double midLaneLength = piece.GetLength(0);

            cs.TraveledMidLaneDistance += cs.Velocity * (midLaneLength / pieceLength);

            // Advance to next piece
            if (cs.PiecePosition.InPieceDistance > pieceLength)
            {
                cs.PiecePosition.PieceIndex = (cs.PiecePosition.PieceIndex + 1) % _track.Pieces.Count;
                cs.PiecePosition.InPieceDistance -= pieceLength;

                if (cs.PiecePosition.PieceIndex == 0)
                {
                    cs.PiecePosition.Lap += 1;
                }

                piece = _track.GetPiece(cs.PiecePosition);

                if (piece.Switch)
                {
                    // Entered switch piece. Targetlane becomes end lane.
                    cs.PiecePosition.Lane = new PiecePositionLane(cs.PiecePosition.Lane.EndLaneIndex,
                        cs.TargetLaneIndex);

                    cs.TargetLaneIndex = cs.TargetLaneIndex2;
                }
                else
                {
                    // Exited normal or switch piece. Just make the start lane equal to end lane.
                    cs.PiecePosition.Lane = new PiecePositionLane(cs.PiecePosition.Lane.EndLaneIndex,
                        cs.PiecePosition.Lane.EndLaneIndex);
                }
            }
        }

        #endregion

        #region Helpers


        /// <summary>
        /// Return 1 if 1 is on front, 2 if 2 is on front, 0 if no collision.
        /// </summary>
        private int PosIntersect(PiecePosition frontPos1, PiecePosition backPos1,
            PiecePosition frontPos2, PiecePosition backPos2)
        {

            if (frontPos1.PieceIndex == backPos1.PieceIndex)
            {
                if (frontPos2.PieceIndex == frontPos1.PieceIndex)
                {
                    if (IsBetween(frontPos2.InPieceDistance, backPos1.InPieceDistance, frontPos1.InPieceDistance))
                    {
                        return 1;
                    }
                }
                else if (backPos2.PieceIndex == frontPos1.PieceIndex)
                {
                    if (IsBetween(frontPos2.InPieceDistance, backPos1.InPieceDistance, frontPos1.InPieceDistance))
                    {
                        return 2;
                    }
                }
            }
            else
            {
                // Front pos is on next piece

                if (backPos2.PieceIndex == frontPos1.PieceIndex)
                {
                    if (backPos2.InPieceDistance < frontPos1.InPieceDistance)
                    {
                        return 2;
                    }
                }
                else if (frontPos2.PieceIndex == backPos1.PieceIndex)
                {
                    if (frontPos2.InPieceDistance > backPos1.InPieceDistance)
                    {
                        return 1;
                    }
                }
                else if (frontPos2.PieceIndex == frontPos1.PieceIndex && backPos2.PieceIndex == backPos1.PieceIndex)
                {
                    if (frontPos1.InPieceDistance > frontPos2.InPieceDistance)
                    {
                        return 1;
                    }
                    else
                    {
                        return 2;
                    }
                }
            }

            return 0;
        }

        private bool IsBetween(double val, double min, double max)
        {
            return val >= min && val <= max;
        }

        private PiecePosition GetFrontPiecePos(Car car, PiecePosition pp)
        {
            var frontPos = pp.InPieceDistance + car.Dimensions.GuideFlagPosition;
            var pieceLength = _track.GetPieceLength(pp);
            var ret = pp.Clone();

            if (frontPos > pieceLength)
            {
                ret.PieceIndex = (ret.PieceIndex + 1) % _track.Pieces.Count;
                ret.InPieceDistance = pieceLength - frontPos;
                ret.Lane = new PiecePositionLane()
                {
                    StartLaneIndex = ret.Lane.EndLaneIndex,
                    EndLaneIndex = ret.Lane.EndLaneIndex
                };
            }
            else
            {
                ret.InPieceDistance = frontPos;
            }

            return ret;
        }

        private PiecePosition GetBackPiecePos(Car car, PiecePosition pp)
        {
            var backPos = pp.InPieceDistance + car.Dimensions.GuideFlagPosition - car.Dimensions.Length;
            var ret = pp.Clone();

            if (backPos < 0)
            {
                ret.PieceIndex -= 1;
                if (ret.PieceIndex == -1) ret.PieceIndex = _track.Pieces.Count - 1;
                var pieceLength = _track.GetPieceLength(pp);
                ret.InPieceDistance = pieceLength + backPos;
                ret.Lane = new PiecePositionLane()
                {
                    StartLaneIndex = ret.Lane.StartLaneIndex,
                    EndLaneIndex = ret.Lane.StartLaneIndex
                };
            }
            else
            {
                ret.InPieceDistance = backPos;
            }

            return ret;
        }

        #endregion
    }
}
