﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace CarBot
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                string host = args[0];
                int port = int.Parse(args[1]);
                string botName = args[2];
                string botKey = args[3];

                Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

                var game = new Game(host, port, botName, botKey, null, null, 1, Game.GameMode.Join);
                var ai = new Ai(game);
                game.Run(ai);

                /*
                string password = "zz1";
                int carCount = 4;
                var game = new Game(host, port, botName, botKey, "green", password, carCount, Game.GameMode.CreateRace);
                var ai = new Ai(game);

                var task1 = System.Threading.Tasks.Task.Factory.StartNew(() => game.Run(ai));
                System.Threading.Thread.Sleep(1500);

                var tasks = new List<System.Threading.Tasks.Task>();
                tasks.Add(task1);

                for (int i = 2; i <= carCount; i++)
                {
                    var game2 = new Game(host, port, botName + i, botKey, null, password, carCount, Game.GameMode.JoinRace);
                    var ai2 = new ConstantVelocityAi(game2, 0.0 + (i-2) * 0.15);
                    var task2 = System.Threading.Tasks.Task.Factory.StartNew(() => game2.Run(ai2));
                    tasks.Add(task2);

                    System.Threading.Thread.Sleep(1000);
                }

                foreach (var task in tasks)
                {
                    task.Wait();
                }
                */
                
#if LOG
                ai.Save();
#endif

                Console.WriteLine("Done");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + "\n" + ex.ToString());
                Debug.WriteLine(ex.Message + "\n" + ex.ToString());
            }

#if LOG
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
#endif
        }
    }
}
