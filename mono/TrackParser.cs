﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace CarBot
{
    public class TrackParser
    {
        private readonly Track _track;
        private readonly int _laps;
        private Part[,] _parts;

        public TrackParser(Race race)
        {
            _track = race.Track;
            _laps = race.RaceSession.Laps;
            if (_laps < 1) _laps = 3;

            Parse();
        }

        public int PartLength
        {
            get { return _parts.GetLength(0); }
        }

        public void Parse()
        {
            _parts = new Part[_track.Pieces.Count * _laps, _track.Lanes.Count];
            int pieceIndex = 0;

            for (int lap = 0; lap < _laps; lap++)
            {
                foreach (var piece in _track.Pieces)
                {
                    int laneIndex = 0;

                    foreach (var lane in _track.Lanes)
                    {
                        _parts[pieceIndex, laneIndex++] =
                            new Part(piece.GetLength(lane.DistanceFromCenter),
                                piece.GetMaxSpeed(lane.DistanceFromCenter),
                                piece.Switch, piece);
                    }

                    pieceIndex++;
                }
            }

            Console.WriteLine("Parsed " + _parts.GetLength(0) + " pieces, " + _track.Lanes.Count + " lanes");
        }

        public int GetPartIndex(PiecePosition position)
        {
            int lap = position.Lap;
            if (lap == -1) lap = 0;
            return lap * _track.Pieces.Count + position.PieceIndex;
        }

        public double GetLaneDistance(PiecePositionLane lane)
        {
            return GetLaneDistance(lane.StartLaneIndex, lane.EndLaneIndex);
        }

        public double GetLaneDistance(int lane1, int lane2)
        {
            var d1 = _track.Lanes[lane1].DistanceFromCenter;
            var d2 = _track.Lanes[lane2].DistanceFromCenter;
            return Math.Abs(d1 - d2);
        }

        public Part GetPart(PiecePosition pos)
        {
            var i = GetPartIndex(pos);
            return GetPart(Math.Min(i, _parts.GetLength(0) - 1), pos.Lane.StartLaneIndex);
        }

        public Part GetNextPart(PiecePosition pos)
        {
            var i = GetPartIndex(pos) + 1;
            return GetPart(Math.Min(i, _parts.GetLength(0) - 1), pos.Lane.EndLaneIndex);
        }

        public Part GetPart(int partIndex, int laneIndex)
        {
            return _parts[partIndex, laneIndex];
        }

        #region Finding parts

        public int? FindNextSwitchPart(int partIndex)
        {
            for (var i = partIndex; i < _parts.GetLength(0); i++)
            {
                if (_parts[i, 0].CanSwitch)
                {
                    return i;
                }
            }

            return null;
        }

        #endregion

        #region Lane calculation

        public double CalculateLaneLength(int laneIndex, int startPartIndex, int endPartIndex)
        {
            double laneLength = 0;

            for (var partIndex = startPartIndex; partIndex <= endPartIndex; partIndex++)
            {
                laneLength += _parts[partIndex, laneIndex].Length;
            }

            return laneLength;
        }

        /*
        public double CalculateLaneTime(int laneIndex, int startPartIndex, int endPartIndex)
        {
            double laneTime = 0;

            for (var partIndex = startPartIndex; partIndex <= endPartIndex; partIndex++)
            {
                var part = _parts[partIndex, laneIndex];
                laneTime += part.Length / (part.v0 + part.Length);
            }

            return laneTime;
        }
        */

        #endregion

        #region Lane choosing

        public int ChooseLane(CarPosition pos)
        {
            double length;
            return ChooseLane(GetPartIndex(pos.PiecePosition) + 1, pos.PiecePosition.Lane.EndLaneIndex, 1, out length);
        }

        public int ChooseLane(int currentSwitchPartIndex, int currentLaneIndex, int maxSteps, out double length)
        {
            Debug.Assert(GetPart(currentSwitchPartIndex, 0).CanSwitch, "Piece is not a switch piece");

            if (maxSteps < 1)
            {
                length = 0;
                return currentLaneIndex;
            }

            int? nextSwitchIndex = FindNextSwitchPart(currentSwitchPartIndex + 1);

            if (nextSwitchIndex.HasValue)
            {
                var lanes = GetPossibleLaneIndexes(currentLaneIndex);
                var shortestLength = double.MaxValue;
                var shortestLaneIndex = -1;

                foreach (var i in lanes)
                {
                    // Get the shortest path from the next switch
                    double len;
                    ChooseLane(nextSwitchIndex.Value, i, maxSteps - 1, out len);

                    if (i != currentLaneIndex)
                    {
                        len += CalculateLaneLength(i, currentSwitchPartIndex + 1, nextSwitchIndex.Value - 1);

                        var part = GetPart(currentSwitchPartIndex, 0);
                        len += part.GetSwitchingLength(_track.Lanes[currentLaneIndex].DistanceFromCenter,
                            _track.Lanes[i].DistanceFromCenter);
                    }
                    else
                    {
                        len += CalculateLaneLength(i, currentSwitchPartIndex, nextSwitchIndex.Value - 1);
                    }

                    if (len < shortestLength)
                    {
                        shortestLength = len;
                        shortestLaneIndex = i;
                    }
                }

                length = shortestLength;
                return shortestLaneIndex;
            }
            else
            {
                length = 0;
                return currentLaneIndex;
            }
        }

        private IEnumerable<int> GetPossibleLaneIndexes(int laneIndex)
        {
            int leftIndex = Math.Max(laneIndex - 1, 0);
            int rightIndex = Math.Min(laneIndex + 1, _track.Lanes.Count - 1);
            return Enumerable.Range(leftIndex, rightIndex - leftIndex + 1);
        }

        #endregion
    }
}
